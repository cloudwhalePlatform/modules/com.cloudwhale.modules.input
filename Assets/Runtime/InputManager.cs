﻿using DG.Tweening;
using I2.Loc;
using System;
using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : Singleton<InputManager>
{
    public static event Action PrimaryDown;
    public static event Action SecondaryDown;
    public static event Action<Coroutine> ManualOverrideStart;
    public static event Action ManualOverrideStop;

#pragma warning disable CS0414, CS0649 // Never used & never assigned warning (unity)
    [Header("Options", order = 4)]
    [SerializeField] float _timerReturnTime = .2f;

    [Header("References", order = 5)]
    [SerializeField] RectTransform _timerBarRect;
    [Space]
    [SerializeField] RectTransform _primaryButtonContainer;
    [SerializeField, ReadOnly] TextMeshProUGUI _primaryText;
    [SerializeField, ReadOnly] Image _primaryFrame;
    [SerializeField, ReadOnly] Image _primaryInner;
    [Space]
    [SerializeField] RectTransform _secondaryButtonContainer;
    [SerializeField, ReadOnly] TextMeshProUGUI _secondaryText;
    [SerializeField, ReadOnly] Image _secondaryFrame;
    [SerializeField, ReadOnly] Image _secondaryInner;

    [Header("Debug", order = 6)]
#if UNITY_EDITOR
    [SerializeField] bool _overrideEditorInput = false;
    [SerializeField] KeyCode _primaryKeyOverride = KeyCode.LeftArrow;
    [SerializeField] KeyCode _secondaryKeyOverride = KeyCode.RightArrow;
    [SerializeField] float _reactionSpeedOverride = 3f;
    [Space]
#endif
    [SerializeField, ReadOnly] bool _blockInput = true;
    [SerializeField, ReadOnly] bool _isInitialized = false;
    [SerializeField, ReadOnly] float _manualTimestamp;
    [Space]
    [SerializeField, ReadOnly] float _currentFillTime = -1;
    [Space]
    [SerializeField, ReadOnly] bool _enablePrimaryHold = false;
    [SerializeField, ReadOnly] bool _isPrimaryHeld = false;

    Coroutine _textUpdateRoutine, _currentTimerRoutine, _activeOverrideCoroutine;
    Sequence _primaryPunchSequence, _secondaryPunchSequence,
        _textChangeSequence, _currentTimerSequence;
#pragma warning restore CS0414, CS0649 // Never used & never assigned warning (unity)

    public float ReactionSpeed =>
#if UNITY_EDITOR
        _overrideEditorInput || WebGLPreferences.Current.ReactionTime <= 0 ? _reactionSpeedOverride :
#endif
        WebGLPreferences.Current.ReactionTime;

    public KeyCode PrimaryKey =>
#if UNITY_EDITOR
        _overrideEditorInput || !WebGLPreferences.Current.HasKeys ? _primaryKeyOverride :
#endif
        WebGLPreferences.Current.PrimaryKey;

    public KeyCode SecondaryKey =>
#if UNITY_EDITOR
        _overrideEditorInput || !WebGLPreferences.Current.HasKeys ? _secondaryKeyOverride :
#endif
        WebGLPreferences.Current.SecondaryKey;

    public bool GetPrimaryKeyDown => Input.GetKeyDown(PrimaryKey) || (_enablePrimaryHold && (Input.GetKey(PrimaryKey) || _isPrimaryHeld));
    public bool GetSecondaryKeyDown => Input.GetKeyDown(SecondaryKey);

    // Start is called before the first frame update
    void OnValidate()
    {
        FindReferences();
    }

    protected override void Awake()
    {
        base.Awake();
        FindReferences();

        KillTimer(_blockInput);
        _currentTimerSequence.Complete();

        CreateFadeTextSequence(_primaryText, 0, 0)
            .Join(CreateFadeTextSequence(_secondaryText, 0, 0))
            .Complete();
    }

    void FindReferences()
    {
        if (_primaryButtonContainer != null)
        {
            _primaryText    = _primaryButtonContainer.GetComponentInChildren<TextMeshProUGUI>();
            _primaryFrame   = _primaryButtonContainer.GetComponentInChildren<Image>();
            _primaryInner   = _primaryFrame.GetComponentsInChildren<Image>().FirstOrDefault(x => x != _primaryFrame);
        }
        if (_secondaryButtonContainer != null)
        {
            _secondaryText  = _secondaryButtonContainer.GetComponentInChildren<TextMeshProUGUI>();
            _secondaryFrame = _secondaryButtonContainer.GetComponentInChildren<Image>();
            _secondaryInner = _secondaryFrame.GetComponentsInChildren<Image>().FirstOrDefault(x => x != _secondaryFrame);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!_blockInput)
        {
            if (GetPrimaryKeyDown)
            {
                TriggerPrimary();
            }
            if (GetSecondaryKeyDown)
            {
                TriggerSecondary();
            }
        }
    }

    public void StartPrimaryHold()
    {
        if (_enablePrimaryHold) _isPrimaryHeld = true;
    }
    public void StopPrimaryHold()
    {
        _isPrimaryHeld = false;
    }

    public void InitTimer(float fillTime = -1)
    {
#if UNITY_EDITOR
        if (_overrideEditorInput)
        {
            _currentFillTime = _reactionSpeedOverride;
            return;
        }
#endif

        if (fillTime > 0 ) _currentFillTime = fillTime;
        else _currentFillTime = ReactionSpeed;
    }

    public static Sequence CreateInputPunchSequence(RectTransform imageRect, RectTransform textRect)
    {
        return DOTween.Sequence()
            .Join(imageRect.DOPunchScale(Vector3.one * .1f, .25f, 1, 0))
            .Join(textRect.DOPunchScale(Vector3.one * .1f, .25f, 1, 0));
    }

    public void TriggerPrimary()
    {
        if (_primaryPunchSequence != null) _primaryPunchSequence.Complete();
        (_primaryPunchSequence = CreateInputPunchSequence(_primaryFrame.rectTransform, _primaryText.rectTransform)).Play();
        PrimaryDown?.Invoke();
    }

    public void TriggerSecondary()
    {
        if (_secondaryPunchSequence != null) _secondaryPunchSequence.Complete();
        (_secondaryPunchSequence = CreateInputPunchSequence(_secondaryFrame.rectTransform, _secondaryText.rectTransform)).Play();
        SecondaryDown?.Invoke();
    }

    public Coroutine SetButtonText(string primaryTextTerm, string secondaryTextTerm, float transitionTime = 1f)
    {
        if (_textUpdateRoutine != null) StopCoroutine(_textUpdateRoutine);
        return _textUpdateRoutine = StartCoroutine(ChangeButtonTextCoroutine(primaryTextTerm, secondaryTextTerm, transitionTime));
    }

    private IEnumerator ChangeButtonTextCoroutine(string primaryTextTerm, string secondaryTextTerm, float transitionTime)
    {
        if (primaryTextTerm == null && secondaryTextTerm == null) yield break;
        _textChangeSequence?.Kill();

        _textChangeSequence = DOTween.Sequence();
        if (_primaryText.text != primaryTextTerm) _textChangeSequence.Join(CreateFadeTextSequence(_primaryText, 0, transitionTime / 2));
        if (_secondaryText.text != secondaryTextTerm) _textChangeSequence.Join(CreateFadeTextSequence(_secondaryText, 0, transitionTime / 2));
        yield return _textChangeSequence.Play().WaitForCompletion();
        
        _primaryText.text = LocalizationManager.GetTranslation("button/" + primaryTextTerm);
        _secondaryText.text = LocalizationManager.GetTranslation("button/" + secondaryTextTerm);

        _textChangeSequence = DOTween.Sequence()
            .Join(CreateFadeTextSequence(_primaryText, 1, transitionTime / 2))
            .Join(CreateFadeTextSequence(_secondaryText, 1, transitionTime / 2))
            .Play();
    }

    public void UpdateTimerBarColor()
    {
        var image = _timerBarRect.GetComponent<Image>();
        if (image.color != OptionsManager.InactiveTimerColor)
            image.DOColor(OptionsManager.DefaultTimerColor, _timerReturnTime);
    }

    private IEnumerator FadeAllText(float fadeLevel, float transitionTime = 1f)
    {
        _textChangeSequence?.Kill();

        _textChangeSequence = CreateFadeTextSequence(_primaryText, fadeLevel, transitionTime)
            .Join(CreateFadeTextSequence(_secondaryText, fadeLevel, transitionTime));
        yield return _textChangeSequence.Play().WaitForCompletion();
    }

    private Sequence CreateFadeTextSequence(TextMeshProUGUI target, float fadeLevel, float transitionTime)
    {
        var sequence = DOTween.Sequence();
        if (target.color.a != fadeLevel)
            sequence.Join(target.DOFade(fadeLevel, transitionTime));

        return sequence;
    }

    public Coroutine StartTimer(string primaryText, string secondaryText, float fillTimerOverride = -1)
    {
        StopTimer();
        SetButtonText(primaryText, secondaryText, OptionsManager.TransitionDuration);

        return _currentTimerRoutine = StartCoroutine(FillTimer(fillTimerOverride));
    }

    private IEnumerator FillTimer(float fillTimerOverride = -1)
    {
        if (_currentFillTime <= 0) InitTimer();

        _blockInput = false;
        _currentTimerSequence = DOTween.Sequence()
            .Append(_timerBarRect.GetComponent<Image>().DOFade(0, _timerReturnTime));
        yield return _currentTimerSequence.Play().WaitForCompletion();

        _timerBarRect.DOScaleX(0, 0).Complete();
        _timerBarRect.GetComponent<Image>().DOColor(OptionsManager.ActiveTimerColor, 0).Complete();
        _timerBarRect.GetComponent<Image>().DOFade(1, 0).Complete();

        _currentTimerSequence = DOTween.Sequence()
            .Append(_timerBarRect.DOScaleX(1, (fillTimerOverride > 0 ? fillTimerOverride : _currentFillTime) - _timerReturnTime));
        yield return _currentTimerSequence.Play().WaitForCompletion();

        StartCoroutine(FadeAllText(0));
        _currentTimerSequence = DOTween.Sequence().Append(_timerBarRect.DOScaleX(1, _timerReturnTime))
            .Join(_timerBarRect.GetComponent<Image>().DOColor(OptionsManager.InactiveTimerColor, _timerReturnTime))
            .Play();
    }

    public void StopTimer(bool blockInput = true, bool fadeButtonText = true, bool setInactive = true)
    {
        _blockInput = blockInput;
        if (fadeButtonText) StartCoroutine(FadeAllText(0));
        if (_currentTimerRoutine != null) StopCoroutine(_currentTimerRoutine);
        KillTimer(setInactive);
    }

    private void KillTimer(bool setInactive)
    {   
        _currentTimerSequence?.Kill();
        _currentTimerSequence = DOTween.Sequence().Append(_timerBarRect.DOScaleX(1, _timerReturnTime))
            .Join(_timerBarRect.GetComponent<Image>().DOColor(
                setInactive 
                ? OptionsManager.InactiveTimerColor 
                : OptionsManager.ActiveTimerColor, 
                _timerReturnTime))
            .Play();       
    }

    public void StartManualOverride(string primaryText, string secondaryText)
    {
        OptionsManager.IsManualOverrideActive = true;
        _manualTimestamp = Time.time;
        StopTimer(true, false, false);

        if (_activeOverrideCoroutine == null)
        {
            SetButtonText(primaryText, secondaryText);
            _activeOverrideCoroutine = StartCoroutine(ManualOverrideCoroutine(primaryText, secondaryText));
            ManualOverrideStart?.Invoke(_activeOverrideCoroutine);
        }
    }

    private IEnumerator StopManualOverrideCoroutine()
    {
        StopManualOverride();
        yield break;
    }

    public void StopManualOverride()
    {
        OptionsManager.IsManualOverrideActive = false;
        StopTimer(true, true, true);

        if (_activeOverrideCoroutine != null)
        {
            StopCoroutine(_activeOverrideCoroutine);
            _activeOverrideCoroutine = null;
            ManualOverrideStop?.Invoke();
        }

        
    }

    private IEnumerator ManualOverrideCoroutine(string primaryTextKey, string secondaryTextKey)
    {
        var isReturnTimerRunning = false;
        while (Time.time < _manualTimestamp + OptionsManager.ManualOverrideTimeout)
        {
            if (Time.time > _manualTimestamp + OptionsManager.ManualReturnTimeOffset)
            {
                if (!isReturnTimerRunning)
                {
                    StartTimer(primaryTextKey, secondaryTextKey, OptionsManager.ManualOverrideTimeout - OptionsManager.ManualReturnTimeOffset);
                    isReturnTimerRunning = true;
                }
            }   
            else if (isReturnTimerRunning)
            {
                StopTimer(false, false);
                isReturnTimerRunning = false;
            }
            yield return null;
            /*if (_isBusy)
            {
                InputManager.Instance.StopTimer();
                yield break;
            }*/
        }

        StartCoroutine(StopManualOverrideCoroutine());
    }
}