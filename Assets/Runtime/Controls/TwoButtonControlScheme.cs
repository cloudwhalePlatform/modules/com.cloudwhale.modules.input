﻿using System;
using UnityEngine;

[Serializable]
public class TwoButtonControlScheme : BaseControlScheme
{
    public BaseInput primaryInput;
    public BaseInput secondaryInput;

    public ButtonControls primaryControls;
    public ButtonControls secondaryControls;

    [SerializeField] bool _enableDown;
    [SerializeField] bool _enableHeld;
    [SerializeField] bool _enableReleased;

    private void Update()
    {
        InvokeCallbacks(primaryInput, primaryControls);
        InvokeCallbacks(secondaryInput, secondaryControls);
    }

    protected override void InvokeCallbacks(BaseInput input, BaseControls controls)
    {
        if (input.GetType() == typeof(ButtonInput) && controls.GetType() == typeof(ButtonControls))
        {
            HandleButtonCallbacks((ButtonInput)input, (ButtonControls)controls);
        }
        else if (input.GetType() == typeof(AxisInput))
        {
            
        }
        else if (input.GetType() == typeof(MouseInput))
        {

        }
    }

    protected void HandleButtonCallbacks(ButtonInput input, ButtonControls controls)
    {
        if (_enableDown && input.IsDown)
        {
            controls.OnDown?.Invoke();
        }

        if (_enableHeld && input.IsHeld)
        {
            controls.OnHeld?.Invoke();
        }

        if (_enableReleased && input.IsReleased)
        {
            controls.OnReleased?.Invoke();
        }
    }
}
