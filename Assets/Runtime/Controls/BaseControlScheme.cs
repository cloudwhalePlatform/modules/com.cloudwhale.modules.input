﻿using System;
using UnityEngine;

[Serializable]
public abstract class BaseControlScheme : MonoBehaviour
{
    protected abstract void InvokeCallbacks(BaseInput input, BaseControls controls);
}
