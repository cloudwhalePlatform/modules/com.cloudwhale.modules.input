﻿using UnityEngine.Events;

public class AxisControls : BaseControls
{
    public event UnityAction OnAxisValueChanged;
    public event UnityAction OnSnappedAxisValueChanged;
}
