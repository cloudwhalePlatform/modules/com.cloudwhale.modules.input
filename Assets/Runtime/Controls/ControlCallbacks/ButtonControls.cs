﻿using UnityEngine.Events;

public class ButtonControls : BaseControls
{
    public UnityEvent OnDown;
    public UnityEvent OnHeld;
    public UnityEvent OnReleased;
}
