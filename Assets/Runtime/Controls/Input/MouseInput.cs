﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MouseInput : BaseInput
{
    public enum MouseType
    {
        Physical,
        Emulated,
        EyeTracking
    }

    [SerializeField] private MouseType _type;
    [SerializeField] private Vector2 _lastPosition;

    public MouseType Type => _type;
    public Vector2 Position => _lastPosition = Input.mousePosition;
    public bool PositionChanged => _lastPosition == (Vector2) Input.mousePosition;
}
