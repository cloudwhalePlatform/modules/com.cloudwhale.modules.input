﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisInput : BaseInput
{
    [Header("Input")]
    [SerializeField] private string _name;
    [SerializeField, ReadOnly] private float _lastValue;

    [Header("Direction/Scaling")]
    [SerializeField] private Vector2 _direction;
    [SerializeField, ReadOnly] private Vector2 _lastDirection;

    [Header("Snapping")]
    [SerializeField] private float _snapInterval;
    [SerializeField, ReadOnly] private float _lastSnappedValue;
    [SerializeField, ReadOnly] private Vector2 _lastSnappedDirection;


    public float AxisValue => _lastValue = Input.GetAxis(_name);
    public Vector2 Direction => _lastDirection = AxisValue * _direction;
    public float SnappedAxisValue => _lastSnappedValue = SnapValueToNextClosestInterval(AxisValue, _snapInterval);
    public Vector2 SnappedDirectionValue => _lastSnappedDirection = SnappedAxisValue * _direction;

    public float SnapValueToNextClosestInterval(float value, float interval)
    {
        var x = Mathf.Round(value / interval);
        return x * interval;
    }

    public bool AxisValueChanged => _lastValue == Input.GetAxis(_name);
    public bool SnappedAxisValueChanged => _lastSnappedValue == SnapValueToNextClosestInterval(AxisValue, _snapInterval);
}
