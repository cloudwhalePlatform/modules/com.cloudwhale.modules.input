﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ButtonInput : BaseInput
{
    [SerializeField] private KeyCode _buttonKeyCode;

    public bool IsDown => Input.GetKeyDown(_buttonKeyCode);
    public bool IsHeld => Input.GetKey(_buttonKeyCode);
    public bool IsReleased => Input.GetKeyUp(_buttonKeyCode);
}
